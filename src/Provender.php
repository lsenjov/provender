<?php

namespace CWE;

require 'constants.php';

use CWE\Libraries\ObjectRex;
use CWE\Libraries\EventEmitter\Interfaces\EventEmitter;
use CWE\Provender\Interfaces\TaskQueue;

class Provender
{
    protected $config = [];
    protected $eventEmitter;
    protected $commands = [];
    protected $plugins = [];
    protected $version = '$Id$';

    public function __construct()
    {
        $this->dotFolderName = PROVENDER_DOT_FOLDER;
    }

    public function run()
    {
        $this->loadPlugins();
    }

    public function &getPlugins()
    {
        return $this->plugins;
    }

    protected function findDotFolder($in = __DIR__)
    {
        // If we have gone as far as the users home directory.
        if (strcmp($_SERVER['HOME'], $in) === 0) {
            return false;
        }

        // Look for provender $in the dir
        $directoryToFind = "$in/.{$this->dotFolderName}";
        if (file_exists($directoryToFind)) {
            // Return the .provender dir we found
            return $directoryToFind;
        }

        $next = dirname($in);
        if ($next == '.' || $next == '/' || strcmp($next, $in) === 0) {
            return false; // We've gone as far as we can
        }

        return $this->findDotFolder($next);
    }

    protected function loadConfig($in)
    {
        $configs = [$this->config];

        // For each config file we find
        foreach (glob("$in/*.yml") as $configFile) {
            if ($yaml = @yaml_parse_file($configFile)) {
                $configs []= $yaml;
                continue;
            }
            $this->getLogger()->logWarn("$configFile could not be parsed as yaml");
        }

        $this->config = call_user_func_array(
            ['CWE\Provender\Helpers', 'arrayMergeRecursive'],
            $configs
        );
    }

    protected function loadPlugins()
    {
        foreach ($this->config['core']['plugins'] as $plugin) {
            try {
                $p = new $plugin($this);
                $this->plugins []= $p;
            } catch (Exception $e) {
                $this->getLogger()->logError($e->message);
            }
        }
    }

    public function registerCommand(ObjectRex $identifier, TaskQueue $command)
    {
        $this->commands []= [$identifier, $command];
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setEventEmitter(EventEmitter &$eventEmitter)
    {
        $this->eventEmitter = $eventEmitter;
    }

    public function &getEventEmitter()
    {
        return $this->eventEmitter;
    }

    public function &getLogger()
    {
        if (is_null($this->logger)) {
            $this->loggerInit();
        }
        return $this->logger;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function loadDefaultConfig()
    {
        $configLocation = __DIR__ . '/Provender/config';
        $this->loadConfig($configLocation);
    }
}

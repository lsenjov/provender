<?php

namespace CWE\Provender\Interfaces;

use CWE\Provender;

interface Plugin
{
    public function getName();
}

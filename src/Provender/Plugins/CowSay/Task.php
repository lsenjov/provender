<?php

namespace CWE\Provender\Plugins\CowSay;

use CWE\Provender\Interfaces;
use CWE\Libraries\EventEmitter\Event;

class Task implements Interfaces\Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    protected $sayings;
    protected $cowAscii;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger =& $resources['logger'];

        if(array_key_exists('sayings', $resources))
        {
            $this->sayings = $resources['sayings'];
        }
        else
        {
            $this->sayings = ["I can't think of anything to say, who's been messing with my code?"];
        }
        $say = $this->sayings[rand(0, count($this->sayings) - 1)];

        if(array_key_exists('cowAscii', $resources))
        {
            $this->cowAscii = $resources['cowAscii'];
        }
        else
        {
            $this->cowAscii = "\n said the wisest cow in the land";
        }

        // Length of the saying for the speech bubble
        $len = strlen($say);

        $this->logger->logInfo(
            "/".str_repeat("-", $len)."\\\n".
            "|".$say."|\n".
            "\\".str_repeat("-", $len)."/\n".
            $this->cowAscii
        );
        $this->logger->log();
        return true;
    }
}

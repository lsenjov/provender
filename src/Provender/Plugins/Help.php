<?php

namespace CWE\Provender\Plugins;

use CWE\Provender;
use CWE\Provender\Command;
use CWE\Provender\Plugins\Help\HelpTask;
use CWE\Provender\Plugins\Help\HelpInformation;
use CWE\Libraries\ObjectRex;
use GetOptionKit\OptionCollection;

class Help
{
    protected $resources = [];

    public function __construct(Provender &$provender)
    {
        $this->addResource('config', $provender->getConfig());
        $this->addResource('eventEmitter', $provender->getEventEmitter());
        $this->addResource('logger', $provender->getLogger());
        $this->resources['provender'] =& $provender;

        $provender->registerCommand(
            new ObjectRex('/^help$/'),
            $this->getCommand()
        );

        $helpinfo = new HelpInformation();
        $provender->getEventEmitter()->addListener(
            'help:help',
            [$helpinfo, 'getHelp'],
            [&$provender]
        );
    }

    protected function addResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    protected function getResources()
    {
        return $this->resources;
    }

    protected function getCommand()
    {
        $resources = $this->getResources();
        $help = new HelpTask($resources);

        $command = new Command($resources);
        $command->setDescription('Shows this help or help for a particular command.');
        $command->addTask($help);
        return $command;
    }
}

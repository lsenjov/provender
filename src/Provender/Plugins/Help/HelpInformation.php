<?php

namespace CWE\Provender\Plugins\Help;

class HelpInformation
{

    public function getHelp($event, &$provender)
    {
        $provender->getLogger()->logInfo(
            'usage: provender <command> <args>'
        );
        $provender->getLogger()->logInfo('');
        $provender->getLogger()->logInfo('List of available commands:');
        $provender->getLogger()->logInfo('');

        $commands = [];
        $longest_command = 0;
        foreach ($provender->getCommands() as $command) {
            $name = str_replace(
                array('/','^','$'),
                '',
                $command[0]->getRegex()
            );
            if (strlen($name)> $longest_command) {
                $longest_command = strlen($name);
            }
            if (!$desc = $command[1]->getDescription()) {
                $desc = "No Description";
            }
            $commands []= [$name, $desc];
        }

        $tablength = 4;

        $longest_command = $tablength*round(
            $longest_command/$tablength
        );
        foreach ($commands as $command) {
            $name = $command[0];
            $desc = $command[1];
            
            $name = str_pad(
                $name,
                $tablength*ceil(strlen($name)/$tablength)
            );
            
            if ((($longest_command-strlen($name))/$tablength) == 0) {
                $name .= "\t";
            } else {
                $name = str_pad(
                    $name,
                    (($longest_command-strlen($name))/$tablength)
                    +strlen($name),
                    "\t"
                );
            }
            $provender->getLogger()->logInfo("\t$name\t- $desc");
        }

        $provender->getLogger()->logInfo('');
        $provender->getLogger()->logInfo(
            'To find out more about a command run `provender help <command>`'
        );

        $provender->getLogger()->logInfo('');
    }
}

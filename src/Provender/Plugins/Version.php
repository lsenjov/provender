<?php

namespace CWE\Provender\Plugins;

use CWE\Provender;
use CWE\Provender\Command;
use CWE\Provender\Plugins\Version\Task;
use CWE\Libraries\ObjectRex;
use GetOptionKit\OptionCollection;

class Version
{
    protected $resources = [];

    public function __construct(Provender &$provender)
    {
        $this->addResource('eventEmitter', $provender->getEventEmitter());
        $this->addResource('logger', $provender->getLogger());
        $this->addResource('version', $provender->getVersion());

        $provender->registerCommand(
            new ObjectRex('/^version$/'),
            $this->getCommand()
        );
    }

    protected function addResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    protected function getResources()
    {
        return $this->resources;
    }

    protected function getCommand()
    {
        $resources = $this->getResources();
        $help = new Task($resources);

        $command = new Command($resources);
        $command->setDescription('Shows the current provender version.');
        $command->addTask($help);
        return $command;
    }
}

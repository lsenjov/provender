<?php

namespace CWE\Provender\Plugins\Version;

use CWE\Provender\Interfaces;
use CWE\Libraries\EventEmitter\Event;

class Task implements Interfaces\Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger =& $resources['logger'];
        $version = $resources['version'];
        $this->logger->logInfo(
            "Current version is: $version"
        );
        $this->logger->log();
        return true;
    }
}

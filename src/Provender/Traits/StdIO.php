<?php

namespace CWE\Provender\Traits;

use CWE\Provender\ConsoleLogger;

trait StdIO
{
    protected $logger;

    protected function loggerInit()
    {
        $this->logger = new ConsoleLogger();
    }
}
